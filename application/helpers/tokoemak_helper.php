<?php

function is_logged_in()
{
    $ci = get_instance();
    if (!$ci->session->userdata('email')) {
        redirect('auth');
    } else {
        $role_id = $ci->session->userdata('role_id');
        $menu = $ci->uri->segment(1);

        $ci->load->model('M_menu');
        $ci->load->model('M_submenu');
        $ci->load->model('M_access_menu');

        $queryMenu = $ci->M_menu->getBy(['menu' => $menu])->row_array();
        $menu_id = $queryMenu['id'];
        if (empty($menu_id)) {
            $querySubMenu = $ci->M_submenu->getBy(['url' => $menu])->row_array();
            $menu_id = $querySubMenu['menu_id'];
        }

        $userAccess = $ci->M_access_menu->getBy([
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ]);

        if ($userAccess->num_rows() < 1) {
            redirect('auth/blocked');
        }
    }
}

function check_access($role_id, $menu_id)
{
    $ci = get_instance();
    $ci->load->model('M_access_menu');

    $ci->db->where('role_id', $role_id);
    $ci->db->where('menu_id', $menu_id);

    $result = $ci->M_access_menu->getBy(['role_id' => $role_id, 'menu_id' => $menu_id]);

    if ($result->num_rows() > 0) {
        return "checked='checked'";
    }
}

function getMenu($idrole)
{
    $ci = get_instance();
    $ci->load->model('M_menu');
    return $ci->M_menu->getMenuByRole($idrole);
}

function setMessage($message, $type)
{
    //type adalah tipe dari alert
    //message pesan nya
    $ci = get_instance();
    $message = $ci->session->set_flashdata('message', '<div class="alert alert-' . $type . ' alert-dismissible fade show" role="alert">' . $message . '               
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>');
}

function setPagination($limit = 10, $rows = null, $url, $model = null, $method = null)
{
    //instance ci
    $ci = get_instance();
    //load library
    $ci->load->library('pagination');

    $config['base_url'] = site_url($url);
    $config['total_rows'] = $rows;
    $config['per_page'] = $limit;

    //styling
    $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
    $config['full_tag_close'] = '</ul></nav>';

    $config['first_link'] = 'First';
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';

    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';

    $config['attributes'] = array('class' => 'page-link');


    //init
    $ci->pagination->initialize($config);
}

function setPaging($table, $url, $perpage, $rows = null, $model = null, $method = null)
{
    //instance ci
    $ci = get_instance();
    //load library
    $ci->load->library('pagination');

    if (!empty($model)) {
        $ci->load->model($model, 'mdl');
        if (empty($method))
            $rows = $ci->mdl->get($ci->mdl->table)->num_rows();
        else
            $rows = $ci->mdl->{$method}()->num_rows();
    } else {
        if (empty($rows) && $rows != 0) {
            $rows = $ci->db->get($table)->num_rows();
        }
    }

    $config['base_url'] = site_url($url);
    $config['total_rows'] = $rows;
    $config['per_page'] = $perpage;

    //styling
    $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
    $config['full_tag_close'] = '</ul></nav>';

    $config['first_link'] = 'First';
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';

    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';

    $config['attributes'] = array('class' => 'page-link');


    //init
    $ci->pagination->initialize($config);
}

function toRupiah($val)
{
    $x = "Rp " . number_format($val, 0, ',', '.');
    return $x;
}

function settingSIM()
{
    $ci = get_instance();
    $ci->load->model('M_setting', 'setting');

    $setting = $ci->setting->get()->result_array();

    $a_data = array();
    foreach ($setting as $val) {
        $a_data[$val['idpengaturan']] = $val['valuepengaturan'];
    }

    return $a_data;
}

function getBarang()
{
    $ci = get_instance();
    $ci->load->model('M_barang', 'barang');

    $barang = $ci->barang->get()->result_array();
    $a_data = array();

    foreach ($barang as $val) {
        $a_data[$val['idbarang']] = $val;
    }

    return $a_data;
}

function sendEmail($subjek, $email, $msg)
{
    $ci = get_instance();

    $config = [
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_user' => 'tokoemakindonesia@gmail.com',
        'smtp_pass' => 'kadalkuro123',
        'smtp_port' => 465,
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'newline' => "\r\n"
    ];

    $ci->load->library('email', $config);
    $ci->email->from('tokoemakindonesia@gmail.com', 'Tokoemak Indonesia');
    $ci->email->to($email);
    $ci->email->subject($subjek);
    $ci->email->message($msg);
    $ok = $ci->email->send();

    return $ok;
}
