<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_barang extends MY_Model
{
    protected $table = 'barang';
    protected $schema = '';
    public $key = 'idbarang';
    public $value = 'namabarang';

    function __construct()
    {
        parent::__construct();
    }

    public function getBarang($limit, $start, $keyword = null)
    {
        $query = "SELECT * FROM barang JOIN jenisbarang USING(idjenis) LIMIT $limit";

        if ($keyword) {
            $this->db->select('*');
            $this->db->from('barang');
            $this->db->join('jenisbarang', 'barang.idjenis=jenisbarang.idjenis');
            $this->db->like('namabarang', $keyword);
            $this->db->limit($limit);
            return $this->db->get()->result_array();
        }

        return $this->db->query($query)->result_array();
    }

    function getRefBy($id)
    {
        $query = "SELECT * FROM barang JOIN jenisbarang USING(idjenis) WHERE idbarang='$id'";
        return $this->db->query($query);
    }

    public function getByKategori($id = null)
    {
        $where = empty($id) ? '' : " WHERE b.idkategori=$id";
        $query = "SELECT * FROM barang b JOIN kategori USING(idkategori) JOIN jenisbarang USING(idjenis)" . $where;

        return $this->db->query($query);
    }

    public function getById($id)
    {
        $query = "SELECT * FROM $this->table JOIN kategori USING(idkategori) JOIN jenisbarang USING(idjenis) WHERE idbarang='$id'";
        return $this->db->query($query);
    }
}
