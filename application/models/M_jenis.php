<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jenis extends MY_Model
{
    protected $table = 'jenisbarang';
    protected $schema = '';
    public $key = 'idjenis';
    public $value = 'namajenis';

    function __construct()
    {
        parent::__construct();
    }

    function getRef()
    {
        $query = "SELECT * FROM jenisbarang JOIN kategori USING(idkategori)";
        return $this->db->query($query);
    }

    function getRefBy($id)
    {
        $query = "SELECT * FROM jenisbarang JOIN kategori USING(idkategori) WHERE idjenis='$id'";
        return $this->db->query($query);
    }
}
