<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MApi_users extends MY_Model
{
    protected $table = 'users';
    protected $schema = '';
    protected $key = 'id';


    public function getUser()
    {
        $user = $this->db->get('users')->result_array();
        return json_encode($user);
    }
}
