<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_supplier extends MY_Model
{
    protected $table = 'supplier';
    protected $schema = '';
    public $key = 'idsupplier';
    public $value = 'namasupplier';

    function __construct()
    {
        parent::__construct();
    }
}
