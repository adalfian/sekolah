<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pembelian extends MY_Model
{
    protected $table = 'pembelian';
    protected $schema = '';
    public $key = 'idpembelian';
    public $value = 'nofaktur';

    function __construct()
    {
        parent::__construct();
    }

    public function getPembelian()
    {
        $query = "SELECT * FROM pembelian JOIN supplier USING(idsupplier)";
        return $this->db->query($query);
    }
}
