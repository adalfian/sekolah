<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pembeliandetail extends MY_Model
{
    protected $table = 'pembelian_detail';
    protected $schema = '';
    public $key = 'idbelidetail';
    public $value = '';

    function __construct()
    {
        parent::__construct();
    }
}
