<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>

        <div class="card shadow mb-4">
            <div class="card-body">
                <button type="button" data-type="tambah" class="btn btn-primary mb-3">Tambah User Baru</button>

                <form action="<?= site_url('menu/user'); ?>" method="post">
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input type="text" name="keyword" class="form-control" placeholder="Cari user ..." aria-label="Cari guru ..." aria-describedby="button-addon4" autocomplete="off" autofocus>
                                <div class="input-group-append" id="button-addon4">
                                    <input class="btn btn-success" type="submit" name="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>

                        <?= $this->session->flashdata('message'); ?>
                        <h5>Hasil : <?= $total_rows; ?></h5>
                        <div class="table-responsive">
                            <form method="post" id="form-list">
                                <table width="100%" class="table table-hover table-striped" id="user-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Username</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Role</th>
                                            <th scope="col">Status</th>
                                            <th scope="col" nowrap align="center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($total_rows == 0) : ?>
                                            <tr class="table table-danger">
                                                <td colspan="7" align="center">Data tidak ditemukan</td>
                                            </tr>
                                        <?php else : ?>
                                            <?php $i = 1; ?>
                                            <?php foreach ($users as $u) : ?>
                                                <?php $is_aktif = ($u['is_aktif'] == 1) ? '' : 'table table-danger'; ?>
                                                <tr class="<?= $is_aktif ?>">
                                                    <th scope="row"><?= ++$start; ?></th>
                                                    <td><?= $u['username']; ?></td>
                                                    <td><?= $u['nama']; ?></td>
                                                    <td><?= $u['email'] ?></td>
                                                    <td><?= $a_role[$u['role_id']]; ?></td>
                                                    <td><?= ($u['is_aktif'] == 1) ? 'Aktif' : 'Tidak Aktif'; ?></td>
                                                    <td>
                                                        <button type="button" data-type="reset" class="btn btn-sm btn-warning" data-id="<?= $u['id']; ?>">Reset</button>
                                                        <?php if ($u['is_aktif'] == 1) : ?>
                                                            <button type="button" data-type="btnstatus" class="btn btn-sm btn-secondary" data-id="<?= $u['id']; ?>" data-status="<?= $u['is_aktif']; ?>">Disable</button>
                                                        <?php else : ?>
                                                            <button type="button" data-type="btnstatus" class="btn btn-sm btn-success" data-id="<?= $u['id']; ?>" data-status="<?= $u['is_aktif']; ?>">Enable</button>
                                                        <?php endif; ?>
                                                        <button type="button" data-type="edit" class="btn btn-sm btn-info" data-id="<?= $u['id']; ?>">Edit</button>
                                                        <button type="button" data-type="btndelete" class="btn btn-sm btn-danger" data-id="<?= $u['id']; ?>">Delete</a>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <input type="hidden" name="act" id="act">
                                <input type="hidden" name="key" id="key">
                            </form>
                            <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Modal -->
        <div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newMenuModalLabel">Tambah User Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?= base_url('menu/user') ?>" method="post" id="modal_post">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password2" name="password2" placeholder="Ulangi Password">
                            </div>
                            <div class="form-group">
                                <select name="role" id="role" class="form-control">
                                    <option>-- Pilih Role --</option>
                                    <?php foreach ($role as $r) : ?>
                                        <option value="<?= $r['id']; ?>"><?= $r['role']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" data-type="simpan" class="btn btn-success">Simpan</button>
                        </div>
                        <input type="hidden" name="act" id="act">
                        <input type="hidden" name="key" id="key">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalReset" tabindex="-1" role="dialog" aria-labelledby="modalResetLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalResetLabel">Reset Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('menu/resetPasswordUser') ?>" method="post" id="modal_post">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password Baru">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password2" name="password2" placeholder="Ulangi Password">
                        </div>
                        <input type="hidden" name="key" id="key">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" data-type="btnreset" class="btn btn-warning">Reset</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus user ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-type="delete" data-id="" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-status">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title">Non-Aktifkan User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="desc">Apakah anda ingin men-Nonaktifkan user ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" data-type="status" data-status="" data-id="" class="">Nonaktifkan</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('[data-type=simpan]').click(function() {
        var act = $('#modal_post #act').val();
        var key = $('#modal_post #key').val();
        if (act == "") {
            $('#modal_post #act').val('simpan');
        }
        $('#modal_post').submit();
    });


    $('[data-type=tambah]').click(function() {
        var modal = $('#newMenuModal');
        $('#modal_post')[0].reset();
        modal.find('#newMenuModalLabel').html('Tambah User Baru');
        modal.find('#password').attr('type', 'password');
        modal.find('#password2').attr('type', 'password');
        modal.modal();
    });


    $('[data-type=btndelete]').click(function() {
        var id = $(this).attr('data-id');
        $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
        $('#modal-delete').modal();
    });

    $('[data-type=delete]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= site_url('menu/deleteUser/') ?>' + id;
    });

    $('[data-type=reset]').click(function() {
        var id = $(this).attr('data-id');
        var modal = $('#modalReset');
        modal.find('#act').val('reset');
        modal.find('#key').val(id);
        modal.modal();
    });

    $('[data-type=btnstatus]').click(function() {
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        var modal = $('#modal-status');
        modal.find('[data-type=status]').attr('data-id', id);
        modal.find('[data-type=status]').attr('data-status', status);

        if (status < 1) {
            modal.find('#title').html('Aktifkan User');
            modal.find('#desc').html('Apakah anda ingin mengaktifkan user ini?');
            modal.find('[data-type=status]').html('Aktifkan');
            modal.find('[data-type=status]').removeClass();
            modal.find('[data-type=status]').addClass('btn btn-success');
        } else {
            modal.find('#title').html('Non-Aktifkan User');
            modal.find('#desc').html('Apakah anda ingin men-Nonaktifkan user ini?');
            modal.find('[data-type=status]').html('Non-Aktifkan');
            modal.find('[data-type=status]').removeClass();
            modal.find('[data-type=status]').addClass('btn btn-danger');
        }
        modal.modal();
    });

    $('[data-type=status]').click(function() {
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        location.href = '<?= site_url('menu/setStatusUser/') ?>' + id + '/' + status;
    });

    $('[data-type=edit]').click(function() {
        var id = $(this).attr('data-id');
        Swal.showLoading();
        xhrfGetData("<?= site_url('menu/getUser/') ?>" + id, function(data) {
            var modal = $('#newMenuModal');
            modal.find('#newMenuModalLabel').html('Ubah User');
            modal.find('#username').val(data.username);
            modal.find('#nama').val(data.nama);
            modal.find('#email').val(data.email);
            modal.find('#role').val(data.role_id);
            modal.find('#password').attr('type', 'hidden');
            modal.find('#password2').attr('type', 'hidden');
            modal.find('#act').val('edit');
            modal.find('#key').val(data.id);
            Swal.close();
            modal.modal();
        });
    });
</script>