<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>
        <div class="card shadow mb-4">
            <div class="card-body">
                <button type="button" class="btn btn-primary mb-3" data-type="tambah">Tambah menu baru</button>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <form method="post" id="form-list">
                            <table class="table table-hover" id="data-table">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Menu</th>
                                        <th scope="col">Icon</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($menu as $m) : ?>
                                        <tr>
                                            <th scope="row"><?= $i; ?></th>
                                            <td><?= $m['menu']; ?></td>
                                            <td><i class="<?= $m['icon']; ?>"></i> <?= $m['icon']; ?></td>
                                            <td>
                                                <button data-type="edit" data-id="<?= $m['id']; ?>" type="button" class="btn btn-sm btn-primary">Edit</button>
                                                <button data-type="btndelete" data-id="<?= $m['id']; ?>" type="button" class="btn btn-sm btn-danger">Hapus</button>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Add New Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('menu') ?>" method="post" id="modal_post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="menu" name="menu" placeholder="Menu name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="icon" name="icon" placeholder="Submenu icon">
                    </div>
                </div>
                <input type="hidden" name="act" id="act">
                <input type="hidden" name="key" id="key">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary" data-type="simpan">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus Menu?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-id="" data-type="delete" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('[data-type=simpan]').click(function() {
        let act = $('#modal_post #act').val();
        let key = $('#modal_post #key').val();

        if (act == "") {
            $('#modal_post #act').val('simpan');
        }

        $('#modal_post').submit();
    });

    $('[data-type=btndelete]').click(function() {
        let id = $(this).attr('data-id');
        $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
        $('#modal-delete').modal();
    });

    $('[data-type=delete]').click(function() {
        let id = $(this).attr('data-id');
        location.href = `<?= base_url('menu/deleteMenu/') ?>${id}`;
    });

    $('[data-type=tambah]').click(function() {
        $('#modal_post')[0].reset();
        $('#newMenuModal').find('#newMenuModalLabel').html('Tambah <?= $title ?>');
        $('#newMenuModal').modal();
    });

    $('[data-type=edit]').click(function() {
        let id = $(this).attr('data-id');
        Swal.showLoading();
        xhrfGetData(`<?= base_url('menu/getMenu/') ?>${id}`, function(data) {
            let modal = $('#newMenuModal');
            modal.find('#newMenuModalLabel').html('Edit Menu');
            modal.find('#menu').val(data.menu);
            modal.find('#icon').val(data.icon);
            modal.find('#act').val('edit');
            modal.find('#key').val(data.id);
            Swal.close();
            modal.modal();
        });
    });

    $('#data-table').DataTable();
</script>