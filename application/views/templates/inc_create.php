<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <!-- <a class="btn btn-primary mb-3" href="<?= $menu . '/create' ?>">Tambah <?= $title; ?></a> -->
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" enctype="multipart/form-data">
                            <?php foreach ($a_kolom as $col) { ?>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <?php if ($col['kolom'] != ':no') : ?>
                                                <label for="<?= $col['kolom'] ?>"><?= $col['label'] ?></label>
                                            <?php endif; ?>
                                            <?php if (isset($col['type'])) {
                                                if ($col['type'] == 'S') { ?>
                                                    <select name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" class="form-control">
                                                        <option value="">--Pilih--</option>
                                                        <?php foreach ($col['option'] as $key => $val) { ?>
                                                            <option value="<?= $key ?>"><?= $val ?></option>
                                                        <?php } ?>
                                                    </select>
                                                <?php
                                                } elseif ($col['type'] == 'A') {
                                                ?>
                                                    <textarea name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" class="form-control" rows="3" placeholder="Masukkan <?= $col['label'] ?>"></textarea>
                                                <?php
                                                } elseif ($col['type'] == 'P') {
                                                ?>
                                                    <input type="password" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" class="form-control" placeholder="Masukkan <?= $col['label'] ?>" />
                                                <?php
                                                } elseif ($col['type'] == 'N') {
                                                ?>
                                                    <input type="number" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" class="form-control" placeholder="Masukkan <?= $col['label'] ?>" />
                                                <?php
                                                } elseif ($col['type'] == 'D') {
                                                ?>
                                                    <input type="date" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" class="form-control" />
                                                <?php
                                                } elseif ($col['type'] == 'F') {
                                                ?>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="<?= $col['kolom'] ?>" name="<?= $col['kolom'] ?>" />
                                                        <label class=" custom-file-label" for="customFile">Pilih File</label>
                                                    </div>
                                                <?php
                                                } else {
                                                ?>
                                                    <input type="text" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" class="form-control" placeholder="Masukkan <?= $col['label'] ?>" />
                                                <?php
                                                }
                                                ?>
                                            <?php
                                            } else {
                                            ?>
                                                <?php if ($col['kolom'] != ':no') : ?>
                                                    <input type="text" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" placeholder="Masukkan <?= $col['label'] ?>" class="form-control" />
                                                <?php endif; ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>