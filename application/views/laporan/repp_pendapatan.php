<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendapatan</title>
</head>

<style>
    .table1 {
        font-family: sans-serif;
        color: #232323;
        border-collapse: collapse;
    }

    .table1,
    .table1 th,
    .table1 td {
        border: 1px solid #000000;
        padding: 8px 20px;
    }

    .table0,
    .table0 th,
    .table0 td {
        border: 0px;
        padding: 8px 20px;
    }
</style>

<body>
    <p align="center">
        <?= settingSIM()['kop_surat'] ?>
    </p>
    <br>
    <table class="table0" style="margin-left: -45px">
        <tr>
            <td>Jenis Laporan</td>
            <td>:</td>
            <td>Pendapatan</td>
        </tr>
        <tr>
            <td>From Date</td>
            <td>:</td>
            <td><?= $from ?></td>
        </tr>
        <tr>
            <td>To Date</td>
            <td>:</td>
            <td><?= $to ?></td>
        </tr>
    </table>
    <br>
    <table class="table1" align="center" width="100%">
        <tr>
            <th>No</th>
            <th>Kode Transaksi</th>
            <th>Tanggal</th>
            <th>Total Pendapatan</th>
        </tr>
        <?php $i = 1;
        $grand_total = 0; ?>
        <?php foreach ($repp_val as $result) : ?>
            <tr>
                <td><?= $i++ ?></td>
                <td><?= $result['kodetransaksi'] ?></td>
                <td><?= $result['tanggal'] ?></td>
                <td><?= toRupiah($result['total']) ?></td>
            </tr>
            <?php $grand_total += $result['total'] ?>
        <?php endforeach; ?>
        <?php if (count($repp_val) < 1) : ?>
            <tr>
                <td colspan="4" align="center">Tidak ada pendapatan</td>
            </tr>
        <?php endif; ?>
        <tr>
            <td colspan="3"><b>Total Seluruh Pendapatan</b></td>
            <td><b><?= toRupiah($grand_total); ?></b></td>
        </tr>
    </table>
    <br>
    <br>
    <table align="right" class="table0" style="margin-right: -50px">
        <tr>
            <td>Mengetahui CEO Tokoemak</td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td align="center"><?= settingSIM()['ceo_name'] ?></td>
        </tr>
    </table>

</body>

</html>