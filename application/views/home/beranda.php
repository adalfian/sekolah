<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">
                <?= $this->session->flashdata('message'); ?>
            </div>
        </div>
        <div class="jumbotron">
            <h1 class="display-4">Selamat datang, <?= $user['nama']; ?></h1>
            <h4><b><?= settingSIM()['app_name'] ?> Point</b> Of <b>Sales</b></h4>
        </div>
        <?php if (!in_array($user['role_id'], array(3, 4))) { ?>
            <!-- Content Row -->
            <div class="row">
                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2" id="barang">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Barang</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $barang; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-folder fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2" id="kategori">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Kategori</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $kategori; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-folder-open fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2" id="user">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Users</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $users; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
</div>
<script>
    $('#barang').click(function() {
        location.href = '<?= site_url('master') ?>';
    });
    $('#kategori').click(function() {
        location.href = '<?= site_url('master/kategori') ?>';
    });
    $('#user').click(function() {
        location.href = '<?= site_url('menu/user') ?>';
    });
</script>