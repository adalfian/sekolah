<h3>Detail User</h3>
<table style="border:1px solid #ccc">
	<tr>
		<td>Username</td>
		<td><?= $users['username'] ?></td>
		<td rowspan="5"><img src="<?= base_url() ?>/assets/usericon.png" style="width:100px;height:100px"></td>
	</tr>
	<tr>
		<td>Nama</td>
		<td><?= $users['nama'] ?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td><?= $users['email'] ?></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="checkbox" <?= $users['isadmin'] ? 'checked' : '' ?>> Sebagai Admin</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="checkbox" <?= $users['isactive'] ? 'checked' : '' ?>> Aktif</td>
	</tr>
</table>

<h4>Riwayat Pendidikan</h4>
<table border="1" style="border: 1px solid #ccc;margin:5px;margin-left:0">
	<tr><th>Pendidikan</th><th>Tahun Masuk</th><th>Tahun Lulus</th><th>Nama Institusi</th></tr>
	<tr><td>TK</td><td>2000</td><td>2001</td><td>TK Biji Sesawi</td></tr>
	<tr><td>SD</td><td>2001</td><td>2005</td><td>SD Tunas Kelapa</td></tr>
	<tr><td>SMP</td><td>2005</td><td>2007</td><td>SMP Pohon Cemara</td></tr>
</table>
<br>