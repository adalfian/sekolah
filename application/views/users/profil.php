<h3>User Profile</h3>
<?php 
if($this->session->flashdata('error')){
	echo '
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		'.$this->session->flashdata('error').'
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
	</div>
	';
}	
if($this->session->flashdata('success')){
	echo '
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		'.$this->session->flashdata('success').'
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
	</div>
	';
}	
?>
<table style="border:1px solid #ccc">
	<?php echo validation_errors(); ?>
	<?php echo form_open('users/index'); ?>
	<?php foreach ($users as $row) {?>
		<tr>
			<td>Username</td>
			<td><input type="text" value="<?= $row['username'] ?>" name="username"></td>
			<td rowspan="5"><img src="<?= base_url('assets/') ?>usericon.png" style="width:100px;height:100px"><br><button>ubah foto</button></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td><input type="text" value="<?= $row['nama'] ?>" name="nama"></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="email" value="<?= $row['email'] ?>" name="email"></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="password" name="password"></td>
		</tr>
		<tr>
			<td>Password confirm</td>
			<td><input type="password" name="passwordconf"></td>
		</tr>
		<tr>
			<td colspan="2"><button>Simpan</button></td>
		</tr>
	<?php }?>
		<input type="hidden" name="iduser" value="<?= $row['iduser'] ?>">
		<input type="hidden" name="act" value="simpan">
	</form>
</table>