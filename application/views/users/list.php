<div class="card">
	<div class="card-header">
		Daftar <?= $title ?>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-12">
				<?php
				if ($this->session->flashdata('error')) {
					echo '
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					' . $this->session->flashdata('error') . '
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				';
				}
				if ($this->session->flashdata('success')) {
					echo '
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					' . $this->session->flashdata('success') . '
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				';
				}
				?>
				<div class="form-row align-items-center">
					<div class="col-sm-3 my-1">
						<label class="sr-only" for="search">Keyword</label>
						<input type="text" class="form-control" id="search" placeholder="Keyword">
					</div>
					<div class="col-sm-3 my-1">
						<button class="btn btn-sm btn-info">Cari</button>
					</div>
					<div class="col-sm-6 my-1">
						<a href="<?= base_url('users/adduser') ?>" class="btn btn-sm btn-success float-right">Tambah</a>
					</div>
				</div>
				<table class="table table-striped">
					<thead class="text-center">
						<tr>
							<th>No.</th>
							<th>Username</th>
							<th>Nama</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 0;
						foreach ($list as $row) { ?>
							<tr>
								<td><?= ++$no; ?></td>
								<td><?= $row['username']; ?></td>
								<td><?= $row['nama']; ?></td>
								<td align="center">
									<a class="btn btn-sm btn-info" href="<?= base_url('users/detailuser/' . $row['iduser']) ?>">Detail</a>
									<a class="btn btn-sm btn-warning" href="<?= base_url('users/edituser/' . $row['iduser']) ?>">Edit</a>
									<?php if ($users['iduser'] != $row['iduser']) { ?>
										<span class="btn btn-sm btn-danger" data-url="<?= base_url('users/deleteuser/' . $row['iduser']) ?>" data-type="hapus">Hapus</span>
									<?php } ?>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="card-footer">
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-center">
				<li class="page-item disabled">
					<a class="page-link" href="#" tabindex="-1">Previous</a>
				</li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item">
					<a class="page-link" href="#">Next</a>
				</li>
			</ul>
		</nav>
	</div>
</div>
<script>
	$('[data-type="hapus"]').click(function() {
		var conf = confirm('Yakin akan menghapus data ini?');
		if (conf) {
			location.href = $(this).attr('data-url');
		}
	});
</script>