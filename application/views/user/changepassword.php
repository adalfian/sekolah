            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>

                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $this->session->flashdata('message'); ?>
                                    <form action="<?= base_url('user/changepassword'); ?>" method="post">
                                        <div class="form-group">
                                            <label for="current_password">Password lama</label>
                                            <input type="password" id="current_password" name="current_password" class="form-control" placeholder="Masukkan password lama">
                                            <?= form_error('current_password', '<small class="text-danger pl-3">', '</small>') ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password1">Password Baru</label>
                                            <input type="password" id="new_password1" name="new_password1" class="form-control" placeholder="Masukkan password baru">
                                            <?= form_error('new_password1', '<small class="text-danger pl-3">', '</small>') ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password">Ulang Password</label>
                                            <input type="password" id="new_password2" name="new_password2" class="form-control" placeholder="Ulangi password">
                                            <?= form_error('new_password2', '<small class="text-danger pl-3">', '</small>') ?>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary"><i class="fas fa-fw fa-key mr-2"></i>Ganti Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->