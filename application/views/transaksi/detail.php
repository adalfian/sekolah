<style>
    .table0,
    .table0 th,
    .table0 td {
        border: 0px;
        padding: 8px 20px;
    }
</style>
<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $sub_title; ?> / <?= $detail_trans[0]['kodetransaksi'] ?></b></h3>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <div class="mb-2">
                            <table class="table0">
                                <tr>
                                    <td><b>Nama</b></td>
                                    <td>:</td>
                                    <td><?= $detail_trans[0]['nama'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>Alamat</b></td>
                                    <td>:</td>
                                    <td><?= $detail_trans[0]['alamat'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>Latitude</b></td>
                                    <td>:</td>
                                    <td><?= $detail_trans[0]['latitude'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>Longitude</b></td>
                                    <td>:</td>
                                    <td><?= $detail_trans[0]['longitude'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>Status Pesanan</b></td>
                                    <td>:</td>
                                    <td><span class="badge badge-<?= $detail_trans[0]['color'] ?>"><?= $detail_trans[0]['status'] ?></span></td>
                                </tr>
                            </table>
                        </div>
                        <form method="post" id="form-list">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">ID barang</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Jenis</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col">Jumlah</th>
                                        <th scope="col">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    $grand_total = 0;
                                    ?>
                                    <?php foreach ($detail_trans as $s) : ?>
                                        <tr>
                                            <td><?= $s['idbarang']; ?></td>
                                            <td><?= $s['namabarang']; ?></td>
                                            <td><?= $s['namajenis']; ?></td>
                                            <td><?= toRupiah($s['harga_jual']); ?></td>
                                            <td><?= $s['jumlah'] ?> pcs</td>
                                            <td><?= toRupiah($s['jumlah'] * $s['harga_jual']); ?></td>
                                        </tr>
                                        <?php $grand_total = $total ?>
                                    <?php endforeach; ?>
                                    <?php if (count($detail_trans) < 1) : ?>
                                        <tr>
                                            <td colspan="7" align="center" class="text-danger">Detail Transaksi tidak ditemukan</td>
                                        </tr>
                                    <?php endif; ?>
                                    <tr class="table table-secondary">
                                        <td colspan="5" align="right"><b>Ongkir</b></td>
                                        <td><b><?= toRupiah($detail_trans[0]['ongkir']) ?></b></td>
                                    </tr>
                                    <tr class="table table-secondary">
                                        <td colspan="5" align="right"><b>Diskon</b></td>
                                        <td><b><?= toRupiah($detail_trans[0]['diskon']) ?></b></td>
                                    </tr>
                                    <tr class="table table-info">
                                        <td colspan="5" align="right"><b>Grand Total</b></td>
                                        <td><b><?= toRupiah($detail_trans[0]['total']) ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    $('[data-type=detail]').click(function() {
        var id = $(this).attr('data-id');
        location.href = `<?= base_url('transaksi/detail/') ?>${id}`;
    });
</script>