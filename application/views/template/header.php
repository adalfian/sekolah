<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <img src="<?= base_url('assets/img/logo.png') ?>" width="30" height="30" class="d-inline-block align-top" alt="">
        SIM Sekolah
    </a>
</nav>
<?php /* ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="<?= base_url('home/') ?>">
            <img width="30px" src="<?= base_url('assets/img/logo.png') ?>">
            Sekolah
        </a>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('home/') ?>">Beranda</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Master</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?= base_url('master/users') ?>">User</a>
                    <a class="dropdown-item" href="<?= base_url('master/siswa') ?>">Siswa</a>
                    <a class="dropdown-item" href="<?= base_url('master/guru') ?>">Guru</a>
                    <a class="dropdown-item" href="<?= base_url('master/kelas') ?>">Kelas</a>
                    <a class="dropdown-item" href="<?= base_url('master/tahunakademik') ?>">Tahun Akademik</a>
                    <a class="dropdown-item" href="<?= base_url('master/jenispelanggaran') ?>">Jenis Pelanggaran</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('home/logout') ?>">Logout</a>
            </li>
        </ul>
    </div>
</nav>
*/ ?>