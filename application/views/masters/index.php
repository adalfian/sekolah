<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>
        <div class="card shadow mb-4">
            <div class="card-body">
                <button type="button" class="btn btn-primary mb-3" data-type="tambah" data-id="tambah">Tambah <?= $title; ?></button>
                <button type="button" class="btn btn-info mb-3" data-type="upload"><i class="fa fa-upload"></i> Import</button>
                <form action="<?= site_url('master'); ?>" method="post">
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input type="text" id="keyword" name="keyword" class="form-control" placeholder="Cari <?= $title; ?>" aria-label="Cari Data Barang ..." aria-describedby="button-addon4" autocomplete="off" autofocus>
                                <div class="input-group-append" id="button-addon4">
                                    <input class="btn btn-success" type="submit" name="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <div class="mb-3">
                            <ul class="nav nav-pills mt-3">
                                <li class="nav-item">
                                    <a class="nav-link <?= $active == 'all' ? 'active' : '' ?>" href="<?= base_url('masters/barang') ?>">Semua</a>
                                </li>
                                <?php foreach ($a_nav as $nav) : ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?= $active == $nav['idkategori'] ? 'active' : '' ?>" href="<?= base_url('masters/barang/index/' . $nav['idkategori']) ?>"><?= $nav['kategori'] ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <form method="post" id="form-list">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">ID Barang</th>
                                        <th scope="col">Nama Barang</th>
                                        <th scope="col">Jenis</th>
                                        <th scope="col">Harpok</th>
                                        <th scope="col">Harjual</th>
                                        <th scope="col">Stok</th>
                                        <th scope="col">Diskon</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($barang as $s) : ?>
                                        <tr <?= $s['is_aktif'] == 0 ? 'class="table table-warning"' : '' ?>>
                                            <td><?= $s['idbarang']; ?></td>
                                            <td><?= $s['namabarang']; ?></td>
                                            <td><?= $s['namajenis']; ?></td>
                                            <td><?= toRupiah($s['harga_pokok']); ?></td>
                                            <td><?= toRupiah($s['harga_jual']); ?></td>
                                            <td><?= $s['stok']; ?></td>
                                            <td align="center"><?= $s['diskon'] . '%'; ?></td>
                                            <td>
                                                <button type="button" data-type="edit" data-id="<?= rawurlencode($s['idbarang']); ?>" class="btn btn-sm btn-primary">Edit</button>
                                                <button type="button" data-type="btndelete" data-id="<?= rawurlencode($s['idbarang']); ?>" class="btn btn-sm btn-danger">Delete</button>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                    <?php if (count($barang) < 1) : ?>
                                        <tr>
                                            <td colspan="8" align="center" class="text-danger">Barang tidak ditemukan</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="modal_upload" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?= base_url('master') ?>" method="post" id="form_upload" enctype='multipart/form-data'>
                <div class="modal-header">
                    <h5 class="modal-title">Import <?= $title; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">File upload</label>
                        <div class="col-md-8">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="fileupload" name="fileupload">
                                <label class="custom-file-label" for="fileupload">Pilih File</label>
                            </div>
                        </div>
                    </div>
                    <p>
                        <button data-type="download" class="btn btn-sm btn-info">Download Template</button>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-type="import" class="btn btn-success">Import</button>
                </div>
                <input type="hidden" name="act" id="act">
                <input type="hidden" name="key" id="key">
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus <?= $title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus <?= $title; ?>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-type="delete" data-id="" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-type=simpan]').click(function() {
        var act = $('#modal_post #act').val();
        var key = $('#modal_post #key').val();
        if (act == "") {
            $('#modal_post #act').val('simpan');
        }
        $('#modal_post').submit();
    });

    $('[data-type=tambah]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= site_url('masters/barang/create') ?>';
    });

    $('[data-type=upload]').click(function() {
        $('#modal_upload').modal();
    });

    $('[data-type=download]').click(function() {
        $('#form_upload #act').val('download');
        $('#form_upload').submit();
    });

    $('[data-type=import]').click(function() {
        $('#form_upload #act').val('import');
        $('#form_upload').submit();
    });

    $('[data-type=btndelete]').click(function() {
        var id = $(this).attr('data-id');
        $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
        $('#modal-delete').modal();
    });

    $('[data-type=delete]').click(function() {
        var id = $(this).attr('data-id');
        location.href = `<?= site_url('masters/barang/deleteBarang/') ?>${id}`;
    });

    $('[data-type=edit]').click(function() {
        var id = $(this).attr('data-id');
        location.href = `<?= site_url('masters/barang/edit/') ?>${id}`;
    });
</script>