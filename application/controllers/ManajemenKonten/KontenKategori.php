<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KontenKategori extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Kategori Konten';
        $this->menu = 'kontenkategori';
        $this->parent = 'manajemenkonten';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_data = [
            '1' => 'Aktif',
            '0' => 'Non-Aktif'
        ];
        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'idkontenkategori', 'label' => 'ID'];
        $a_kolom[] = ['kolom' => 'kontenkategori', 'label' => 'Kategori Konten'];
        $a_kolom[] = ['kolom' => 'is_aktif', 'label' => 'Aktif', 'type' => 'S', 'option' => $a_data];

        $this->a_kolom = $a_kolom;
    }
}
