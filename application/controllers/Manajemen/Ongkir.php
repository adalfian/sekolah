<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ongkir extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Ongkir';
        $this->menu = 'ongkir';
        $this->parent = 'manajemen';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {


        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'idongkir', 'label' => 'ID Ongkir'];
        $a_kolom[] = ['kolom' => 'namaongkir', 'label' => 'Nama Ongkir'];
        $a_kolom[] = ['kolom' => 'biaya', 'label' => 'Biaya', 'type' => 'N', 'set_currency' => true];

        $this->a_kolom = $a_kolom;
    }
}
